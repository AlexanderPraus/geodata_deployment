---
qgis_settings:
  # QGIS Settings
  helm_chart_name: webgis-qgisserver
  helm_release_name: geodata-qgisserver

  # Name of the GitLab project where we store all files to build QGIS Docker image.
  QGIS_CUSTOM_GITLAB_PROJECT: "{{ inv_gd.qgis.image_project }}"

  # Helm sepcific settings
  qgis_data_dir: "/data/qgis/basis"
  nginx_default_conf: "/etc/nginx/conf.d/"
  nginx_qgis_conf: "qgis-nginx.conf"
  nginx_server_name: "mapserver.{{ DOMAIN }}"
  nginx_cors_origin: "*" 
  #"https://{{ inv_gd.masterportal.appname }}.{{ DOMAIN }}"

  # QGIS Server
  server_replicaCount: 1
  server_image_registry: "registry.gitlab.com"
  server_image_repository: "{{ inv_gd.qgis.image_project }}"
  server_image_tag: "{{ inv_gd.qgis.image_tag }}"
  server_image_pullPolicy: Always
  server_image_pullSecrets: "gitlab-registry"
  server_service_port: 5555
  server_target_port: 5555

  # QGIS-HTTP-Server
  http_replicaCount: 1
  http_service_port: 80
  http_target_port: 80
  http_service_nodePort: 30080

  # QGIS environment variables
  # env_QGIS_LANG: "en_EN.UTF-8"
  env_QGIS_PROJECT_FILE: "project.qgs"
  env_QGIS_SERVER_LOG_LEVEL: "0"
  env_DEBUG: "1"

  # QGIS: values for file 'pg_service.conf'
  postgis_service: "qwc_geodb"
  postgis_events_service: "qwc_events"
  postgis_port: 5432
  postgis_sslmode: "disable"

  # QGIS: values for file 'pg_service.conf'
  timescale_service: "timescale"
  timescale_port: 5432
  timescale_sslmode: "enable"

  # PostGIS: MountPath and Name of 'pg_service.conf' for QGIS Server
  pgservicefile_dir: "/data/postgresql"
  pgservicefile: "qgis-pg-service.conf"

  # Ingress settings
  ingress_enabled: false
  ingress_host: "mapserver.{{ DOMAIN }}"
  ingress_host_path: "/"
  ingress_tls_secret: "mapserver.{{ DOMAIN }}-tls"

masterportal_settings:
  helm_chart_name: webgis-masterportal
  helm_release_name: geodata-masterportal
  helm_release_name_beta: geodata-betaportal

  replicaCount: 1
  image_registry: "registry.gitlab.com/"
  image_repository: "{{ inv_gd.masterportal.image_project }}"
  image_tag: "{{ inv_gd.masterportal.image_tag }}"
  image_tag_testing: "{{ inv_gd.masterportal.image_tag_testing }}"
  image_pullPolicy: Always
  beta_image_repository: "{{ inv_gd.betaportal.image_project }}"
  beta_image_tag: "{{ inv_gd.betaportal.image_tag }}"

  service_type: ClusterIP
  service_port: 80

  ingress_enabled: false
  ingress_host: "{{ inv_gd.masterportal.appname }}.{{ inv_gd.domain }}"
  ingress_host_path: "/"
  ingress_tls_secret: "{{ inv_gd.masterportal.appname }}.{{ inv_gd.domain }}-tls"

mp_oauth_settings:
  helm_repo_name: k8s-at-home
  helm_repo_url: https://oauth2-proxy.github.io/manifests
  helm_chart_name: oauth2-proxy
  helm_release_name: mpoauth2-proxy

  image_registry: "docker.io"
  image_repository: "bitnami/oauth2-proxy"
  image_tag: "7.1.3"
  image_pullPolicy: IfNotPresent

  pvc_enabled: true
  pvc_size: "1Gi"

## IDM Clients
IDM_CLIENT:
  MASTERPORTAL: "masterportal"
  GRAVITEE: "gravitee"
  API_ACCESS: "api-access"
  MASTERPORTAL_PUBLIC: "public_masterportal"

am_apim_settings:
  oauth_resource_name: 'keycloak-auth'
  oauth_plan_name: 'keycloak-plan'
  ## APIS
  ### Geodata
  geodata_api_path: "/fiware/ngsi"
  geodata_api_portal_name: 'FIWARE-NGSI v2 Orion Context Broker'
  geodata_api_portal_description: 'API for the Orion Context Broker, imported by ansible.'

idm_keycloak_settings:
  ## IDM Scopes
  idm_keycloak_scope_TENANT_NAMES: "tenant-names"
  idm_keycloak_scope_TENANT_VALUES: "tenants"
  idm_keycloak_scope_API_READ: "api:read"
  idm_keycloak_scope_API_WRITE: "api:write"
  idm_keycloak_scope_API_DELETE: "api:delete"

mapfish:
  image_registry: "registry.gitlab.com"
  image_repository: "berlintxl/futr-hub/platform/geodata_customizing/mapfish_print"
  image_tag: "{{ inv_gd.mapfish.image_tag }}"
  image_tag_testing: "{{ inv_gd.mapfish.image_tag_testing }}"
  image_pullPolicy: Always  

geoserver_settings:
  helm_chart_name: "geoserver"
  helm_release_name: "txlgeoserver"
  helm_chart_reference: "files/helmcharts/geoserver"
  helm_values_file: "templates/geoserver/values.yaml"
  image_registry: "docker.io"
  image_repository: "kartoza/geoserver"
  image_tag: "2.22.0"

citydb_settings:
  wfs:
    image_registry: "docker.io"
    image_repository: "3dcitydb/wfs"
    image_tag: "5.3.1"